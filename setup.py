#!/usr/bin/python
#
# Copyright (C) 2012-2016  Codethink Limited


from distutils.core import setup
import glob


setup(name='lorry-controller',
      description='Run Lorry on remote repositories in a controller manner',
      author='Codethink Ltd',
      author_email='baserock-dev@baserock.org',
      url='http://www.baserock.com/',
      scripts=['lorry-controller-webapp', 'lorry-controller-minion',
               'lorry-controller-remove-old-jobs'],
      packages=['lorrycontroller', 'lorrycontroller.migrations'],
      data_files=[
        ('share/lorry-controller/templates', glob.glob('templates/*')),
        ('share/lorry-controller/static', glob.glob('static/*')),
        ],
     )
