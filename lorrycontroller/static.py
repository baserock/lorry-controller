# Copyright (C) 2014  Codethink Limited
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; version 2 of the License.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.


import logging

import bottle

import lorrycontroller


class StaticFile(lorrycontroller.LorryControllerRoute):

    # Note that the path below must match what lighttpd (running on a
    # different port than us) would accept.

    http_method = 'GET'
    path = '/lc-static/<filename>'

    def run(self, **kwargs):
        logging.info('%s %s called', self.http_method, self.path)
        return bottle.static_file(
            kwargs['filename'],
            self.app_settings['static-files'])
