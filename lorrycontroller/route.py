# Copyright (C) 2014  Codethink Limited
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; version 2 of the License.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.


import lorrycontroller


class LorryControllerRoute(object):

    '''Base class for Lorry Controller HTTP API routes.

    A route is an HTTP request that the Bottle web application
    recognises as satisfied by a particular callback. To make it
    easier to implement them and get them added automagically to a
    Bottle instance, we define the callbacks as subclasses of this
    base class.

    Subclasses MUST define the attributes ``http_method`` and
    ``path``, which are given the bottle.Bottle.route method as the
    arguments ``method`` and ``path``, respectively.

    '''

    def __init__(self, app_settings, templates):
        self.app_settings = app_settings
        self._templates = templates
        self._statedb = None

    def open_statedb(self):
        return lorrycontroller.StateDB(self.app_settings['statedb'])

    def run(self, **kwargs):
        raise NotImplementedError()
