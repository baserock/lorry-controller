<!DOCTYPE HTML>
<html>
    <head>
        <title>Lorry Controller: Job {{job_id}}</title>
        <link rel="stylesheet" href="/lc-static/style.css" type="text/css" />
    </head>
    <body>
<h1>Status of job {{job_id}}</h1>
<p>Path of git repo: <code>{{path}}</code></p>
<p>Started: {{job_started}}</p>
<p>Ended: {{job_ended}}</p>
<p>MINION: <code>{{host}}:{{pid}}</code></p>
<p>Exit code: <code>{{exit}}</code></p>
<p>Lorry disk usage (after job's finished): {{disk_usage_nice}}</p>
<p>Output:</p>
<pre>{{output}}</pre>
<hr />
<p>Updated: {{timestamp}}</p>
    </body>
</html>
