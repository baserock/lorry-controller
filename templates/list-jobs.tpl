<!DOCTYPE HTML>
<html>
    <head>
        <title>Lorry Controller: ALL the jobs</title>
        <link rel="stylesheet" href="/lc-static/style.css" type="text/css" />
    </head>
    <body>
        % import json

        <h1>ALL the jobs</h2>

<table>
<tr>
<th>Job ID</th>
<th>path</th>
<th>exit?</th>
</tr>
% for job in job_infos:
<tr>
<td><a href="/1.0/job-html/{{job['job_id']}}">{{job['job_id']}}</a></td>
<td><a href="/1.0/lorry-html/{{job['path']}}">{{job['path']}}</a></td>
<td>{{job['exit']}}</td>
</tr>
% end
</table>

        <hr />

        <p>Updated: {{timestamp}}</p>

    </body>
</html>
